import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

const ReactRoulette = ({
  items,
  colors,
  frameColor,
  centerColor,
  image,
  onChange,
  clickSpeed,
  friction,
}) => {
  const elRueda = useRef(null);
  const elTail = useRef(null);

  const dmp = 1 - friction;

  const itemDeg = 360 / items.length;

  const pointerThreshold = 0.2;

  const requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame;

  window.requestAnimationFrame = requestAnimationFrame;

  let animation = null;
  let position = 0;
  let speed = 0;
  let rolling = false;

  let dragging = false;
  let _mousePosition = null;
  let mousePosition = null;
  let centerPosition;

  const rollingMath = () => {
    if (rolling && speed <= 0.05) {
      rolling = false;
      speed = 0;
      const selected = items.length - Math.floor(position / itemDeg) - 1;
      onChange(items[selected]);
    }
  };

  const draggingMath = () => {
    if (dragging && _mousePosition && mousePosition && centerPosition) {
      speed = 0;
      const _angle = Math.atan2(
        _mousePosition.y - centerPosition.y,
        _mousePosition.x - centerPosition.x,
      );
      const angle = Math.atan2(
        mousePosition.y - centerPosition.y,
        mousePosition.x - centerPosition.x,
      );
      position += (angle - _angle) * (180 / Math.PI) * 1.5;
    }
  };

  const arrowAnimation = () => {
    const porcentajeEnItem = (position % itemDeg) / itemDeg;
    const diff =
      porcentajeEnItem > 1 - pointerThreshold
        ? Math.abs((1 - pointerThreshold - porcentajeEnItem) / pointerThreshold)
        : 0;
    elTail.current.style.transform = `rotate(-${diff * 25}deg) translateX(-50%)`;
  };

  useEffect(() => {
    const step = () => {
      if (position > 360) position -= 360;
      if (position < 0) position += 360;
      elRueda.current.style.transform = `rotate(${position + 5}deg)`;
      position += speed;
      speed *= dmp;

      rollingMath();
      draggingMath();

      arrowAnimation();

      animation = window.requestAnimationFrame(step);
    };

    animation = window.requestAnimationFrame(step);

    const onPressed = (e) => {
      e.stopPropagation();
      e.preventDefault();
      if (rolling) return;
      dragging = true;
      const elemRect = elRueda.current.getBoundingClientRect();
      // prettier-ignore
      centerPosition = {
        x: elemRect.left + (elemRect.width / 2),
        y: elemRect.top + (elemRect.height / 2),
      };
      // prettier-ignore-end
      mousePosition = {
        x: ((e.touches || [])[0] || e).clientX,
        y: ((e.touches || [])[0] || e).clientY,
      };
    };

    const onMove = (e) => {
      if (!rolling) {
        e.stopPropagation();
        e.preventDefault();
        const actual = {
          x: ((e.touches || [])[0] || e).clientX,
          y: ((e.touches || [])[0] || e).clientY,
        };
        _mousePosition = mousePosition || actual;
        mousePosition = actual;
      }
    };

    const onRelease = (e) => {
      if (!rolling) {
        e.stopPropagation();
        e.preventDefault();

        if (!centerPosition || !mousePosition || !_mousePosition) return;
        if (
          Math.abs(mousePosition.x - _mousePosition.x) < 16 &&
          Math.abs(mousePosition.y - _mousePosition.y) < 16
        ) {
          return;
        }

        dragging = false;
        rolling = true;

        const _angle = Math.atan2(
          _mousePosition.y - centerPosition.y,
          _mousePosition.x - centerPosition.x,
        );
        const angle = Math.atan2(
          mousePosition.y - centerPosition.y,
          mousePosition.x - centerPosition.x,
        );
        speed = (angle - _angle) * (180 / Math.PI) * 1.5;
      }
    };

    elRueda.current.addEventListener('mousedown', onPressed);
    elRueda.current.addEventListener('touchstart', onPressed);

    document.addEventListener('mousemove', onMove);
    document.addEventListener('touchmove', onMove);

    document.addEventListener('mouseup', onRelease);
    document.addEventListener('touchend', onRelease);

    return () => {
      window.cancelAnimationFrame(animation);
      elRueda.current.removeEventListener('mousedown', onPressed);
      elRueda.current.removeEventListener('touchstart', onPressed);

      document.removeEventListener('mousemove', onMove);
      document.removeEventListener('touchmove', onMove);

      document.removeEventListener('mouseup', onRelease);
      document.removeEventListener('touchend', onRelease);
    };
  }, [elRueda]);

  const handleOnClick = () => {
    rolling = true;
    // prettier-ignore
    speed = clickSpeed + (Math.random() * 10);
    // prettier-ignore-end
  };

  return (
    <div className="ReactRoulette">
      <div ref={elRueda} className="rueda">
        <button type="button" className="center" onClick={handleOnClick}>
          {image && <img src={image} alt="" />}
        </button>
      </div>
      <div ref={elTail} className="tail" />
      <style jsx>
        {`
          position: relative;
          height: 400px;
          width: 400px;

          .rueda {
            z-index: 1;
            width: 100%;
            height: 100%;
            border-radius: 100%;
            border: 14px solid ${frameColor};
            background: conic-gradient(
              ${items.map((item, i) =>
                  `${colors[i % colors.length]} ${itemDeg * i}deg ${itemDeg * (i + 1)}deg`)}
            );
          }

          .center {
            z-index: 2;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 25%;
            height: 25%;
            border-radius: 100%;
            background: ${centerColor};
            transform: translate(-50%, -50%);
            border: 2px solid ${frameColor};

            img {
              width: 100%;
              height: 100%;
              object-fit: contain;
            }
          }

          .tail {
            z-index: 2;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 32px 18px 0 18px;
            border-color: ${frameColor} transparent transparent transparent;
            position: absolute;
            top: 7px;
            left: 50%;
            transform: translateX(-50%);
            transform-origin: top left;

            img {
              width: 100%;
              height: 100%;
            }
          }

          @media (max-width: 480px) {
            height: 320px;
            width: 320px;
          }

          @media (max-width: 320px) {
            height: 220px;
            width: 220px;
          }
        `}
      </style>
    </div>
  );
};

ReactRoulette.propTypes = {
  items: PropTypes.arrayOf(PropTypes.string).isRequired,
  colors: PropTypes.arrayOf(PropTypes.string).isRequired,
  frameColor: PropTypes.string,
  centerColor: PropTypes.string,
  image: PropTypes.string,
  clickSpeed: PropTypes.number,
  friction: PropTypes.number,
  onChange: PropTypes.func,
};

ReactRoulette.defaultProps = {
  frameColor: '#FFF',
  centerColor: '#333',
  image: undefined,
  clickSpeed: 50,
  friction: 0.005,
  onChange: () => {},
};

export default ReactRoulette;
