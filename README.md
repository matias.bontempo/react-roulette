# React Roulette

This is a very simple React component wich allows you to display an interactive roulette that can be thrown and it returns the selected value to be used.

![Demo](https://matias.bontempo.gitlab.io/react-roulette/assets/images/demo-2.gif)

## Installation and usage

You can use this component by installing with npm:

```
npm i react-roulette
```

Then import it in your app:

```
import React from 'react';
import Roulette from 'react-roulette';

const App = () => {
	const items = ['Apple', 'Banana', 'Cherry'];
	const colors = ['#F76156', '#FBD1A2', '#BED558'];

	return (
		<div className="App">
			<h1>React Roulette</h1>
			<Roulette items={items} colors={colors} />
		</div>
	);
}

export default App;
```

## Props

Required props:

- `items` - Array of items to display.
- `colors` - Array of colors to use. If it's smaller tha than the items it's used in loop.

Optional props:

- `frameColor` - Color for the frame and the arrow. Default: `white`.
- `centerColor` - Color for the center button. Default: `#333`.
- `image` - Url of an image to use inside the center button.
- `clickSpeed` - Speed to rotate the roulette when the center button is clicked. Default: `50`.
- `friction` - Friction applied to the rotation. Default: `0.005`.
- `onChange` - Function triggered when the roulette stops. Returns the element where it landed as an argument.
